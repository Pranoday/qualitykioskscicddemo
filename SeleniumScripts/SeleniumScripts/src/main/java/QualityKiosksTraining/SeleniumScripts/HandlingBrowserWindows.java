package QualityKiosksTraining.SeleniumScripts;

import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingBrowserWindows {

	public static void main(String[] args) throws InterruptedException 
	{
	
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.get("https://letskodeit.teachable.com/p/practice");
		
		WebElement OpenWindowButton=D.findElementById("openwindow");
		OpenWindowButton.click();
		Thread.sleep(5000);
		String OriginalWindowHandle=D.getWindowHandle();
		
		System.out.println("Original Window "+D.getWindowHandle());
		
		Set<String>Handles=D.getWindowHandles();
		/*
		 * Handles=487485745848fngngfdgn
		 * 		cvmgmfgmfg484845857485
		 */
		for(String Handle:Handles)
		{
			D.switchTo().window(Handle);
			String Title=D.getTitle();
			
			if(Title.equals("Let's Kode It"))
			{
				break;
			}	
		}	
		
		
		WebElement LoginLink=D.findElementByPartialLinkText("Login");
		LoginLink.click();
		Thread.sleep(5000);
		D.close();
		Thread.sleep(5000);
		D.switchTo().window(OriginalWindowHandle);
		System.out.println(D.getTitle());
		D.quit();
		
	}

}
