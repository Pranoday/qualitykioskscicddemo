package QualityKiosksTraining.SeleniumScripts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ConfirmationboxHandling {

	public static void main(String[] args) 
	{
		
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.get("https://letskodeit.teachable.com/p/practice");
		
		WebElement NameField=D.findElementById("name");
		NameField.sendKeys("Pranoday");
		
		String Name=NameField.getAttribute("value");
		
		WebElement ConfirmBtn=D.findElementById("confirmbtn");
		ConfirmBtn.click();
		
		Alert Al=D.switchTo().alert();
		
		String ConfirmText=Al.getText();
		if(ConfirmText.contains(Name))
			System.out.println("Name is present in Confirmation box...PASSED");
		else
			System.out.println("Name is NOT present in Confirmation box...FAILED");
		Al.dismiss();
		
		
		
		

	}

}
