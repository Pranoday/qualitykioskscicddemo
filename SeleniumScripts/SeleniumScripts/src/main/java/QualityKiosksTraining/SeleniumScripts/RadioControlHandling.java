package QualityKiosksTraining.SeleniumScripts;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RadioControlHandling {

	public static void main(String[] args) 
	{
		
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.get("https://letskodeit.teachable.com/p/practice");
		
		WebElement RadioElement=D.findElementById("bmwradio");
		RadioElement.click();
		
		boolean SelectedOrNot=RadioElement.isSelected();
		
		if(SelectedOrNot==true)
			System.out.println("Radio button got selected after clicking..PASSED");
		else
			System.out.println("Radio button did got selected after clicking..PASSED");

		WebElement HondaRadio=D.findElementById("hondaradio");
		HondaRadio.click();
		
		if(HondaRadio.isSelected())
			System.out.println("Honda radio got selected after clicking..PASSED");
		else
			System.out.println("Honda radio did not get selected after clicking..PASSED");
		
	}

}
