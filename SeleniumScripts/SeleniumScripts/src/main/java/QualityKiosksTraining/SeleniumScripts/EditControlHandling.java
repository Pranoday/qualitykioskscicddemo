package QualityKiosksTraining.SeleniumScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class EditControlHandling {

	public static void main(String[] args) 
	{
		
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.get("https://letskodeit.teachable.com/p/practice");
		
		D.manage().window().maximize();
		
		
		
		WebElement HideButton=D.findElementById("hide-textbox");
		HideButton.click();
		
		WebElement NameField=D.findElementByName("show-hide");
		boolean IsShown=NameField.isDisplayed();
		
		if(IsShown==false)
			System.out.println("Hide button works fine..PASSED");
		else
			System.out.println("Hide button does not work fine..FAILED");
		
		WebElement ShowButton=D.findElementById("show-textbox");
		ShowButton.click();
		
		IsShown=NameField.isDisplayed();
		if(IsShown==true)
			System.out.println("Show button works fine..PASSED");
		else
			System.out.println("Show button does not work fine..FAILED");
		
		
	}

}
