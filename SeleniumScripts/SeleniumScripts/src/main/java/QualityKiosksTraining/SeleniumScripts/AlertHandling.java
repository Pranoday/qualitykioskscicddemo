package QualityKiosksTraining.SeleniumScripts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertHandling {

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.get("https://letskodeit.teachable.com/p/practice");
		
		WebElement NameField=D.findElementByName("enter-name");
		NameField.sendKeys("Pranoday");
		
		String EnteredName=NameField.getAttribute("value");
		
		WebElement AlertBtn=D.findElementById("alertbtn");
		AlertBtn.click();
		
		Alert Al=D.switchTo().alert();
		String AlertMessage=Al.getText();
		
		if(AlertMessage.contains(EnteredName))
			System.out.println("Entered name is present in Alert message..PASSED");
		else
			System.out.println("Entered name is NOT present in Alert message..FAILED");
		
		Al.accept();
	}

}
