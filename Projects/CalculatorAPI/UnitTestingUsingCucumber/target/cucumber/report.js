$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('FeatureFiles\TestAdditionFunctionality.feature');
formatter.feature({
  "line": 1,
  "name": "Testing Addition functionality of Calculator API",
  "description": "",
  "id": "testing-addition-functionality-of-calculator-api",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Checking Addition functionality with Positive Numbers",
  "description": "",
  "id": "testing-addition-functionality-of-calculator-api;checking-addition-functionality-with-positive-numbers",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "CalculatorAPI class object is created",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Addition function is called with 10 and 20",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "Addition function should return 30",
  "keyword": "Then "
});
formatter.match({
  "location": "AdditionStepDefinations.CreateClassObj()"
});
formatter.result({
  "duration": 371811600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 33
    },
    {
      "val": "20",
      "offset": 40
    }
  ],
  "location": "AdditionStepDefinations.TestingAdditionWithPositiveNumbers(int,int)"
});
formatter.result({
  "duration": 1767600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "30",
      "offset": 32
    }
  ],
  "location": "AdditionStepDefinations.CheckingResult(int)"
});
formatter.result({
  "duration": 3007200,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Checking Addition with  0 and Positive Number",
  "description": "",
  "id": "testing-addition-functionality-of-calculator-api;checking-addition-with--0-and-positive-number",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "CalculatorAPI class object is created",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "Addition function is called with 0 and 100",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Addition function should return 100",
  "keyword": "Then "
});
formatter.match({
  "location": "AdditionStepDefinations.CreateClassObj()"
});
formatter.result({
  "duration": 21500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 33
    },
    {
      "val": "100",
      "offset": 39
    }
  ],
  "location": "AdditionStepDefinations.TestingAdditionWithPositiveNumbers(int,int)"
});
formatter.result({
  "duration": 150000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 32
    }
  ],
  "location": "AdditionStepDefinations.CheckingResult(int)"
});
formatter.result({
  "duration": 74000,
  "status": "passed"
});
});