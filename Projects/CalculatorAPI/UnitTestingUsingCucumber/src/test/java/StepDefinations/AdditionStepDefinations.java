package StepDefinations;

import org.testng.Assert;

import QualityKiosksTraining.UnitTestingUsingCucumber.Calculator;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class AdditionStepDefinations 
{
	Calculator Cal;
	int Res=0;
	@Given("^CalculatorAPI class object is created$")
	public void CreateClassObj()
	{
		Cal=new Calculator();
	}

	
	@When("^Addition function is called with (\\d+) and (\\d+)$")
	//@When("^Addition function is called with 10 and 20$")
	public void TestingAdditionWithPositiveNumbers(int Num1, int Num2) {
	//public void TestingAdditionWithPositiveNumbers() {
	    
	    Res=Cal.Addition(Num1, Num2);
	 }
	
	@Then("^Addition function should return (\\d+)$")
	public void CheckingResult(int ExpectedOutput) {
	    // Express the Regexp above with the code you wish you had
	    
	    Assert.assertEquals(Res, ExpectedOutput,"Addition function does not work with Positive numbers");
	}

	
}
