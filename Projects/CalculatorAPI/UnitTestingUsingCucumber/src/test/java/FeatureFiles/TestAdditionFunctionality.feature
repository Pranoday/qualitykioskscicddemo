Feature: Testing Addition functionality of Calculator API

	Scenario: Checking Addition functionality with Positive Numbers
		Given CalculatorAPI class object is created
		When Addition function is called with 10 and 20
		Then Addition function should return 30
	
	Scenario: Checking Addition with  0 and Positive Number
	 Given CalculatorAPI class object is created		
	 When Addition function is called with 0 and 100
	 Then Addition function should return 100